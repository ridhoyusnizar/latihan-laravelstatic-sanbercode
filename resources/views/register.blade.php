<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Halaman Pendaftaran</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h4>Sign Up Form</h4>
  <form action="/welcome" method="POST">
    @csrf
    <label for="first_name">First name: </label> <br /><br />
    <input type="text" name="first_name" id="first_name" /> <br /><br />
    <label for="last_name">Last name: </label> <br /><br />
    <input type="text" name="last_name" id="last_name" /><br /><br />
    <label for="">Gender:</label><br /><br />
    <input type="radio" name="gender" value="0" /> Male <br /><br />
    <input type="radio" name="gender" value="1" /> Female <br /><br />
    <input type="radio" name="gender" value="2" /> Other <br /><br />
    <label for="">Nationality:</label> <br /><br />
    <select>
      <option>Indonesian</option>
      <option>Malaysian</option>
      <option>Singaporean</option>
    </select>
    <br />
    <br />
    <label for="">Language Spoken:</label> <br /><br />
    <input type="checkbox" name="language" value="0" /> Bahasa Indonesia
    <br /><br />
    <input type="checkbox" name="language" value="1" /> English <br /><br />
    <input type="checkbox" name="language" value="2" /> Other <br /><br />
    <label for="bio">Bio:</label> <br /><br />
    <textarea name="bio" name="bio" cols="30" rows="10" id="bio"></textarea>
    <br /><br />
    <button type="submit">Sign Up</button> <br /><br />
    </>
</body>

</html>